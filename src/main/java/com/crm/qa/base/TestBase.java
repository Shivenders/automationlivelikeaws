package com.crm.qa.base;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.DateFormat;
import java.util.Base64;
import java.util.HashMap;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.ScreenOrientation;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
//import com.crm.qa.util.TestUtil;
//import com.crm.qa.util.WebEventListener;
import com.crm.qa.util.CapabilityReader;
import com.crm.qa.util.ConfigReader;
import com.ssts.pcloudy.Connector;
import com.ssts.pcloudy.dto.file.PDriveFileDTO;
import com.ssts.pcloudy.exception.ConnectError;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.touch.offset.PointOption;

public class TestBase {
	private static final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	public static EventFiringWebDriver e_driver;
	public static AppiumDriver<MobileElement> driver;
	public static AppiumDriver<MobileElement> driver1;
	public static DesiredCapabilities capabilities;
	public static DesiredCapabilities capabilities1;
	public static ExtentReports reports;
	public static ExtentTest testInfo;
	public static ExtentHtmlReporter htmlReporter;
	public static Properties prop = null;
	public static String alertjson = null;
	public static String cheermeterjson = null;
	public static String emojijson = null;
	public static String imagepolljason = null;
	public static String imagepredictionjason = null;
	public static String imagequizjson = null;
	public static String textpolljson = null;
	public static String textpredictjson = null;
	public static String textquizjson = null;
	public static String result1 = "";
	public static String exception = null;
	public static AppiumDriverLocalService service;
	
	public static String folder_name;
	public static DateFormat df;
	PDriveFileDTO uploadedApp;
	File destination;
	String app;
	
	/*
	 * @BeforeSuite(alwaysRun = true) public void setup() throws IOException,
	 * ConnectError { URL url = new URL(System.getProperty("appLink"));
	 * System.out.println(System.getProperty("appLink")); String background =
	 * randomAlphaNumeric(10); app=background+".ipa"; destination = new File(app);
	 * System.out.println(app); // Copy bytes from the URL to the destination file.
	 * FileUtils.copyURLToFile(url, destination); Connector con = new
	 * Connector("https://device.pcloudy.com"); String authToken =
	 * con.authenticateUser("shivender@livelike.com", "27j5xgqvtz3nq9z3s8p5qh27");
	 * uploadedApp = con.uploadAndResignIpaFile(authToken, destination, true );
	 * System.out.println("initial==="+uploadedApp.file); }
	 */
	
	
	
	@Parameters({"deviceName","version"})
	@BeforeMethod
	public void setUpSuite(String deviceName, String version) throws Exception {
		
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability("pCloudy_Username", "vivek@livelike.com");
		capabilities.setCapability("pCloudy_ApiKey", "3jtp8w37x2z8vxvhkhqrd2qx");
		capabilities.setCapability("pCloudy_ApplicationName", System.getProperty("appLink"));		
		capabilities.setCapability("pCloudy_DurationInMinutes", 5);
		//capabilities.setCapability("pCloudy_DeviceManafacturer", deviceName);
		//capabilities.setCapability("pCloudy_DeviceVersion", "10.3.2");
		capabilities.setCapability("pCloudy_DeviceFullName", deviceName);
		//capabilities.setCapability("platformVersion", "10.3.2");
		capabilities.setCapability("platformVersion", version);
		capabilities.setCapability("newCommandTimeout", 600);
		capabilities.setCapability("launchTimeout", 90000);
		capabilities.setCapability("bundleId", "com.livelike.msdk.test.ios");
		capabilities.setCapability("acceptAlerts", true);
		capabilities.setCapability("automationName", "XCUITest");
		driver = new IOSDriver(new URL("https://device.pcloudy.com/appiumcloud/wd/hub"), capabilities);
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		
}

	/*
	 * public static void initialization() throws MalformedURLException,
	 * InterruptedException {
	 * 
	 * System.out.println("Im in intialization-testbase");
	 * 
	 * capabilities = new DesiredCapabilities();
	 * capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "iOS"); ////
	 * capabilities.setCapability(MobileCapabilityType.NO_RESET, true); ////
	 * capabilities.setCapability("platformVersion", "12.1");
	 * capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "auto");
	 * capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "XCUITest");
	 * capabilities.setCapability("udid", "auto"); ////
	 * capabilities.setCapability(MobileCapabilityType.UDID, ////
	 * "e19acffeed3b53d802d11aaf8c131c8758c4d7b3"); ////
	 * capabilities.setCapability(MobileCapabilityType.APP,
	 * "/Users/shivendersingh/git/FreeCRMTest/screenshots/Native_iOS_Pilot_3.ipa");
	 * capabilities.setCapability("bundleId", "com.livelike.msdk.test.ios"); ////
	 * capabilities.setCapability("unicodeKeyboard", true); ////
	 * capabilities.setCapability("resetKeybord", true);
	 * capabilities.setCapability("newCommandTimeout", 600);
	 * capabilities.setCapability("clearSystemFiles", true);
	 * 
	 * driver = new IOSDriver<MobileElement>(new
	 * URL("http://localhost:4723/wd/hub"), capabilities);
	 * driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
	 * 
	 * }
	 */
	
	
	public static String randomAlphaNumeric(int count) {
		StringBuilder builder = new StringBuilder();
		while (count-- != 0) {
			int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
			builder.append(ALPHA_NUMERIC_STRING.charAt(character));
		}
		return builder.toString();
	}

	public void manageRooms() {
		// TODO Auto-generated method stub

		MobileElement el5 = (MobileElement) driver.findElementById("Manage Joined Rooms");
		el5.click();
		MobileElement el6 = (MobileElement) driver.findElementByXPath(
				"//XCUIElementTypeAlert[@name=\"Manage Joined Rooms\"]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther");
		el6.sendKeys("dba595c6-afab-4f73-b22f-c7c0cb317ca9");
		MobileElement el7 = (MobileElement) driver.findElementById("Add Room");
		el7.click();
		MobileElement el8 = (MobileElement) driver.findElementById("Manage Joined Rooms");
		el8.click();
		MobileElement el9 = (MobileElement) driver.findElementByXPath(
				"//XCUIElementTypeAlert[@name=\"Manage Joined Rooms\"]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther");
		el9.sendKeys("f05ee348-b8e5-4107-8019-c66fad7054a8");
		MobileElement el10 = (MobileElement) driver.findElementById("Add Room");
		el10.click();

	}

	public static void iostestapp() throws InterruptedException {
		// TODO Auto-generated method stub

		driver.findElementByXPath(
				"//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[2]/XCUIElementTypeStaticText")
				.click();

		MobileElement el2 = (MobileElement) driver.findElementById("PROD");
		el2.click();
		
		Thread.sleep(6000);

		driver.findElementById("Video Layout").click();
		

	}

	public static void testappLaunch() {

		MobileElement el1 = (MobileElement) driver.findElementById("com.livelike.livelikedemo:id/events_label");
		el1.click();
		MobileElement el2 = (MobileElement) driver.findElementById("android:id/text1");
		el2.click();
		MobileElement el3 = (MobileElement) driver.findElementById("com.livelike.livelikedemo:id/themes_label");
		el3.click();
		MobileElement el4 = (MobileElement) driver.findElementByXPath(
				"/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.TextView[1]");
		el4.click();

		MobileElement el5 = (MobileElement) driver.findElementById("layout side");
		el5.click();

	}
	
	
	@AfterMethod
	public void endTest() throws  IOException {

		driver.quit();
	}

}

package com.crm.qa.pagesTestApp;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.time.Duration;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.JavascriptExecutor;
import org.apache.commons.io.FileUtils;
import org.json.JSONException;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.ScreenOrientation;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import com.crm.qa.base.TestBase;

import com.crm.qa.widgets.TextQuizApi;
import com.crm.qa.widgetsAndroid.TextQuizApiAndroid;

import io.appium.java_client.HasOnScreenKeyboard;
import io.appium.java_client.MobileElement;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.StartsActivity;
import io.appium.java_client.touch.LongPressOptions;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class HomePageTApp extends TestBase {
	private static final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	// static final String
	// inputString="BTv?'%Zpfqc,9.C#Mbdk4>CJ2mpmbobCOgb^xdvOP.^m%1(Zk>r"Tv0T|AT|PfqbtB$nlxdWkV%!|AxrMoT^iXs#w#oYDBH4'D8gwIu<Y(kr,8A/,yHiRUUm39|S#m5owjl2L9McsNdk4D@X.@TK";

	HomePageTApp homePage;
	static Response response;
	static JsonPath jsonPathEvaluator;
	static WebDriver driver2;
	@FindBy(xpath = "//XCUIElementTypeOther[@name=\"Video\"]")
	WebElement video;

	@FindBy(xpath = "//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[5]/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeTextField")
	WebElement chatEditfield;

	// XCUIElementTypeApplication[@name="LiveLike
	// Demo"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[5]/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeTextField

	@FindBy(id = "disclosure indicator")
	WebElement sendButton;

	@FindBy(xpath = "//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[5]/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[50]")
	WebElement lastchat;

	@FindBy(xpath = "//XCUIElementTypeStaticText[@name=\"😒😄😇😂❤️👨🏻‍✈️\"]")
	WebElement emoji;

	@FindBy(id = "chat emoji button")
	WebElement stickerbtn;

	@FindBy(xpath = "//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[4]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeCollectionView[1]/XCUIElementTypeCell[2]/XCUIElementTypeOther/XCUIElementTypeImage")
	WebElement stickermidoptionbutton;

	@FindBy(xpath = "//XCUIElementTypeStaticText[@name=\"￼￼￼￼￼:raptors1::raptors2::raptors4:\"]")
	WebElement stickerlastchat;

	/// XCUIElementTypeApplication[@name="LiveLike
	/// Demo"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[5]/XCUIElementTypeOther[1]/XCUIElementTypeOther
	@FindBy(xpath = "//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[5]/XCUIElementTypeOther[1]/XCUIElementTypeOther")
	WebElement chatbox;

	@FindBy(xpath = "//XCUIElementTypeStaticText[@type=\"XCUIElementTypeStaticText\"]")
	WebElement videotime;

	// Initializing the Page Objects:
	public HomePageTApp() {
		PageFactory.initElements(driver, this);
	}

	public Boolean verifyHomePage() throws InterruptedException {

		if (driver.findElementsByXPath(
				"//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[5]/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell")
				.size() > 0) {
			MobileElement el4 = (MobileElement) driver.findElementById("Video");
			System.out.println("Video found");
		}
		return true;

	}

	public boolean verifyChat() throws InterruptedException {

		int size = driver.findElementsByXPath(
				"//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[5]/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell")
				.size();
		if (size > 0) {
			System.out.println("Chat Loaded!!!!");
			chatEditfield.click();
			String background = randomAlphaNumeric(10);
			chatEditfield.sendKeys(background);

			MobileElement el1 = (MobileElement) driver.findElementById("chat send button");
			el1.click();
			System.out.println("Chat sent" + background);
			driver.findElementByXPath("//XCUIElementTypeStaticText[@value=\"" + background+ "\"]");	
			return true;
		}
		
		else {
			System.out.println("Chats not loaded");
			return false;
		}

		

	}

	public boolean verifyEmoji() throws InterruptedException, MalformedURLException {

		int size = driver.findElementsByXPath(
				"//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[5]/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell")
				.size();
		if (size > 0) {
			chatEditfield.click(); // String ballEmoji = "\u26BD"+"\uD83D\uDE01"; //
			String ballEmoji = "\uD83D"; // String ballEmoji = "\u26BD";
			chatEditfield.sendKeys("\u26BD\u26BE\u26c4");
			MobileElement el1 = (MobileElement) driver.findElementById("chat send button");
			el1.click();

			// Thread.sleep(3000);

			/*
			 * RemoteWebElement element = (RemoteWebElement) driver.findElementByXPath(
			 * "//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[5]/XCUIElementTypeOther[1]/XCUIElementTypeOther"
			 * ); String elementID = element.getId(); HashMap<String, String> scrollObject =
			 * new HashMap<String, String>(); scrollObject.put("element", elementID);
			 * 
			 * scrollObject.put("direction", "down"); driver.executeScript("mobile:scroll",
			 * scrollObject); // Thread.sleep(1000);
			 */
			size = size + 1;
			boolean flag = false;
			for (int i = 1; i <= 3; i++) {
				String text = driver.findElementByXPath(
						"//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[5]/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell["
								+ size + "]/XCUIElementTypeStaticText[" + i + "]")
						.getText();
				System.out.println(text);
				if (text.equals("⚽⚾⛄")) {
					flag = true;
				}
			}
			if (flag == true) {
				System.out.println("Chat found");
				return true;
			} else {
				System.out.println("Last Chat found");
				return false;
			}
		}
		System.out.println("Chat not loaded");
		return false;
	}

	public boolean verifySticker() throws InterruptedException {
		Thread.sleep(5000);
		int size = driver.findElementsByXPath(
				"//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[5]/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell")
				.size();
		if (size > 0) {
			MobileElement el1 = (MobileElement) driver.findElementById("textfield_chat_input");
			el1.click();
			el1.sendKeys(":raptors1::raptors2::raptors4:");
			MobileElement send = (MobileElement) driver.findElementById("chat send button");
			send.click();
			MobileElement el3 = (MobileElement) driver.findElementById("textfield_chat_input");
			el3.sendKeys(":raptors1:");

			send.click();
			boolean flag = false;
			size = size + 1;
			for (int i = 1; i <= 3; i++) {
				String text = driver.findElementByXPath(
						"//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[5]/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell["
								+ size + "]/XCUIElementTypeStaticText[" + i + "]")
						.getText();
				System.out.println(text);
				if (text.equals("")) {
					flag = true;
				}
			}
			if (flag == true) {
				System.out.println("Strng matched");
				return true;
			} else {
				System.out.println("Strng not matched");
				return false;
			}
		}
		System.out.println("Chat not loaded");
		return false;
	}

	public boolean verify_Mix_Emoji_Sticker_Text() throws InterruptedException {
		Thread.sleep(5000);
		int size = driver.findElementsByXPath(
				"//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[5]/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell")
				.size();
		if (size > 0) {
			String background = randomAlphaNumeric(10);
			System.out.println(background);
			MobileElement e1l1 = (MobileElement) driver.findElementById("textfield_chat_input");
			e1l1.click();
			Thread.sleep(15000);
			e1l1.sendKeys(background + "\u26BD\u26BE\u26c4:raptors1::raptors2::raptors4:");
			MobileElement send = (MobileElement) driver.findElementById("chat send button");
			send.click();

			size = size + 1;
			boolean flag = false;
			for (int i = 1; i <= 3; i++) {
				String text = driver.findElementByXPath(
						"//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[5]/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell["
								+ size + "]/XCUIElementTypeStaticText[" + i + "]")
						.getText();
				System.out.println(text);
				String compare = background + "⚽⚾⛄";
				System.out.println(compare);
				if (text.equalsIgnoreCase(compare)) {
					flag = true;
				}
			}

			if (flag == true) {
				System.out.println("Strng matched");
				return true;
			} else {
				System.out.println("Strng not matched");
				return false;
			}
		}
		System.out.println("Chat not loaded");
		return false;

	}

	public boolean verifyScrolling() throws InterruptedException {
		// TODO Auto-generated method stub
		Thread.sleep(5000);
		int size = driver.findElementsByXPath(
				"//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[5]/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell")
				.size();
		if (size > 0) {
			String background = randomAlphaNumeric(10);
			chatEditfield.sendKeys(background);
            System.out.println("chat sent"+background);
			MobileElement send = (MobileElement) driver.findElementById("chat send button");
			send.click();
			RemoteWebElement element = (RemoteWebElement) driver.findElementByXPath(
					"//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[5]/XCUIElementTypeOther[1]/XCUIElementTypeOther");
			String elementID = element.getId(); // System.out.println(elementID);
			HashMap<String, String> scrollObject = new HashMap<String, String>();
			scrollObject.put("element", elementID); // Only for ‘scroll in element’
			scrollObject.put("direction", "up");
			driver.executeScript("mobile:scroll", scrollObject);
			driver.executeScript("mobile:scroll", scrollObject);
			driver.executeScript("mobile:scroll", scrollObject);
			Thread.sleep(2000);
			size = size + 1;
			boolean flag = false;
			
		if(driver.findElementByXPath(
					"//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[5]/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell["
							+ size + "]/XCUIElementTypeStaticText").isDisplayed()){
				flag=true;
			} 
		
			if (flag == true) {
				System.out.println("Strng matched");
				return false;
			} else {
				System.out.println("Strng not matched");
				return true;
			}
		}
		System.out.println("Chat not loaded");
		return false;
	}

	public boolean Background_Chat_Load() throws MalformedURLException, InterruptedException {
		
		boolean fl = false;
		int size = driver.findElementsByXPath(
				"//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[5]/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell")
				.size();
		if (size > 0) {

			MobileElement el1 = (MobileElement) driver.findElementById("textfield_chat_input");
			el1.click();
			String background = randomAlphaNumeric(10);
			el1.sendKeys(background);
			MobileElement send = (MobileElement) driver.findElementById("chat send button");
			send.click();
			Thread.sleep(1000);
			driver.runAppInBackground(Duration.ofSeconds(10));
			driver.findElementByXPath("//XCUIElementTypeStaticText[@value=\"" + background+ "\"]");	
			background = randomAlphaNumeric(10);
			el1.sendKeys(background);
			send.click();
			driver.findElementByXPath("//XCUIElementTypeStaticText[@value=\"" + background+ "\"]");	
           return true;
		}
		return false;
	}

	public static String randomAlphaNumeric(int count) {
		StringBuilder builder = new StringBuilder();
		while (count-- != 0) {
			int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
			builder.append(ALPHA_NUMERIC_STRING.charAt(character));
		}
		return builder.toString();
	}
//	public void clickOnNewContactLink(){
//		Actions action = new Actions(driver);
//		action.moveToElement(contactsLink).build().perform();
//		newContactLink.click();
//		
//	}

	public boolean Duplicate_Chat_Load() throws InterruptedException {

		
		int xpathCount = 0;
		int size = driver.findElementsByXPath(
				"//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[5]/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell")
				.size();
		if (size > 0) {
			chatEditfield.click();
			String background = randomAlphaNumeric(10);
			System.out.println(background);
			chatEditfield.sendKeys(background);
			MobileElement send = (MobileElement) driver.findElementById("chat send button");
			send.click();
			xpathCount=driver.findElementsByXPath("//XCUIElementTypeStaticText[@value=\"" + background+ "\"]").size();
			System.out.println("number of last chat"+xpathCount);
		}
		if(xpathCount>1)
		{
			System.out.println("Duplicate found");
			return false;
		}
		else
			System.out.println("Duplicate not found");
			return true;

	}

	public boolean Chat_Queueing_Bypass() throws InterruptedException, MalformedURLException, ParseException {
		// TODO Auto-generated method stub

		/*
		 * chatEditfield.click(); String background = randomAlphaNumeric(10);
		 * chatEditfield.sendKeys(background); sendButton.click(); //
		 * (newTouchAction(driver)).tap(298, 300).perform(); (new
		 * //TouchAction(driver)).tap(new PointOption().withCoordinates(298,
		 * //300)).perform(); Thread.sleep(3000); RemoteWebElement element =
		 * (RemoteWebElement) driver.findElementByXPath(
		 * "//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[5]/XCUIElementTypeOther[1]/XCUIElementTypeOther"
		 * ); String elementID = element.getId(); HashMap<String, String> scrollObject =
		 * new HashMap<String, String>(); scrollObject.put("element", elementID); //
		 * //Only for ‘scroll in element’ scrollObject.put("direction", "down");
		 * driver.executeScript("mobile:scroll", scrollObject);
		 * driver.executeScript("mobile:scroll", scrollObject); Thread.sleep(1000);
		 * boolean tt = driver.findElementByXPath("//XCUIElementTypeStaticText[@name=\""
		 * + background + "\"]").isEnabled();
		 */

		return true;
	}

	public boolean Video_Buffering() throws InterruptedException, IOException {
		// TODO Auto-generated method stub

		int size = driver.findElementsByXPath(
				"//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[5]/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell")
				.size();
		if (size > 0) {
		String text=driver.findElementById("label_video_timer").getText();
		System.out.println(text);
        Thread.sleep(5000);
        String text1=driver.findElementById("label_video_timer").getText();
        System.out.println(text1);
        
        if(text.equals("text1"))
        {
        	return false;
        }
        else {
        	return true;
        }
		}
		System.out.println("Chat not loading");
		return false;
		
	}

	public boolean Video_Buffering_Landscape() throws InterruptedException, IOException {
		// TODO Auto-generated method stub
		// Thread.sleep(10000);
		
		int size = driver.findElementsByXPath(
				"//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[4]/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell")
				.size();
		System.out.println(size);
		if (size > 0) {
		String text=driver.findElementById("label_video_timer").getText();
		System.out.println(text);
        Thread.sleep(5000);
        String text1=driver.findElementById("label_video_timer").getText();
        System.out.println(text1);
        
        if(text.equals(text1))
        {
        	return false;
        }
        else {
        	return true;
        }
		}
		System.out.println("Chat not loading");
		return false;
	}

	public boolean Video_Resume() throws InterruptedException, IOException {
		/*
		 * try { RequestSpecification request = RestAssured.given();
		 * request.header("Content-Type", "application/json"); request.header("Referer",
		 * "https://producer.livelikecdn.com/apps/It40FlkcwAxpIgdMQxlRlQODkl4TbfkWdYo8tNAb/programs/643673fa-ca7c-4dd2-b264-418390ff3e19"
		 * ); request.header("Origin", "https://producer.livelikecdn.com");
		 * request.header("User-Agent",
		 * "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36"
		 * ); request.header("Content-Type", "application/json"); for (int i = 0; i < 3;
		 * i++) { Thread.sleep(5000); response =
		 * request.get("https://cf-streams.livelikecdn.com/live/patriots/index.m3u8");
		 * int statusCode = response.getStatusCode(); System.out.println("Statuscode = "
		 * + statusCode); Assert.assertEquals(statusCode, 200); TakesScreenshot ts =
		 * (TakesScreenshot) driver; File source = ts.getScreenshotAs(OutputType.FILE);
		 * // capture screenshot file File target = new File(
		 * System.getProperty("user.dir") + "/screenshots/" + "Video_Buffering_" + i +
		 * ".png");
		 * 
		 * FileUtils.copyFile(source, target); String screenshotPath =
		 * System.getProperty("user.dir") + "\\screenshots\\" + "Video_Buffering_" + i +
		 * ".png"; testInfo.pass("Screenshot is below:" +
		 * testInfo.addScreenCaptureFromPath(screenshotPath));
		 * System.out.println("screenshot captured"); }
		 * 
		 * driver.runAppInBackground(Duration.ofSeconds(10)); // driver.launchApp();
		 * System.out.println("Launch app again"); response =
		 * request.get("https://cf-streams.livelikecdn.com/live/patriots/index.m3u8");
		 * int statusCode = response.getStatusCode(); System.out.println("Statuscode = "
		 * + statusCode); Assert.assertEquals(statusCode, 200); // Thread.sleep(5000); }
		 * catch (AssertionError e) {
		 * 
		 * System.out.println("Exception is:" + e);
		 * 
		 * }
		 */

		return true;
	}

	public boolean verifySnaptoLive() throws InterruptedException {
		
		int size = driver.findElementsByXPath(
				"//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[5]/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell")
				.size();
		if (size > 0) {

			chatEditfield.click();
			String background = randomAlphaNumeric(10);
			chatEditfield.sendKeys(background);
			System.out.println(background);
			MobileElement el1 = (MobileElement) driver.findElementById("chat send button");
			el1.click();

			

			RemoteWebElement element = (RemoteWebElement) driver.findElementByXPath(
					"//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[5]/XCUIElementTypeOther[1]/XCUIElementTypeOther");
			String elementID = element.getId();

			HashMap<String, String> scrollObject = new HashMap<String, String>();
			scrollObject.put("element", elementID); // Only for ‘scroll in element’
			scrollObject.put("direction", "up");
			driver.executeScript("mobile:scroll", scrollObject);
			driver.executeScript("mobile:scroll", scrollObject);
			driver.executeScript("mobile:scroll", scrollObject);

			// Thread.sleep(1000);
			driver.findElementByXPath(
					"//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[5]/XCUIElementTypeButton")
					.click(); // Thread.sleep(2000); return
			
			driver.findElementByXPath("//XCUIElementTypeStaticText[@value=\"" + background+ "\"]");	
			return true;
		}
		System.out.println("chat not loaded");
		return false;
	}

	public boolean VerifyVideoLandscape_HideChatTest() throws InterruptedException {
		// TODO Auto-generated method stub
		int size = driver.findElementsByXPath(
				"//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[4]/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell")
				.size();
		System.out.println(size);
		if (size > 0) {
			Point a=driver.findElementById("Video").getCenter();
			int x=a.getX();
			int y=a.getY();
			(new TouchAction(driver)).tap(new PointOption().withCoordinates(x,y)).perform();
			size = driver.findElementsByXPath(
					"//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[4]/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell")
					.size();
			
			if(size>0)
			{
				System.out.println("Chat not hidden");
				return false;
			}
			else {
				System.out.println("Chat hidden");
				return true;
			}
		}
		System.out.println("Chat not loaded");
		return true;
	}

	public boolean VerifyChat_Moderation() throws MalformedURLException, InterruptedException {
		// TODO Auto-generated method stub
		/*
		 * chatEditfield.click(); String background = randomAlphaNumeric(10);
		 * chatEditfield.sendKeys(background); sendButton.click(); // (new
		 * TouchAction(driver)).tap(298, 300).perform(); (new
		 * TouchAction(driver)).tap(new PointOption().withCoordinates(298,
		 * 300)).perform(); Thread.sleep(3000); RemoteWebElement element =
		 * (RemoteWebElement) driver.findElementByXPath(
		 * "//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[5]/XCUIElementTypeOther[1]/XCUIElementTypeOther"
		 * ); String elementID = element.getId(); HashMap<String, String> scrollObject =
		 * new HashMap<String, String>(); scrollObject.put("element", elementID); //
		 * Only for ‘scroll in element’ scrollObject.put("direction", "down");
		 * driver.executeScript("mobile:scroll", scrollObject);
		 * driver.executeScript("mobile:scroll", scrollObject); Thread.sleep(1000);
		 * boolean tt = driver.findElementByXPath("//XCUIElementTypeStaticText[@name=\""
		 * + background + "\"]").isEnabled();
		 */

		return true;

	}

	public boolean Chat_Reaction() throws InterruptedException {
		// TODO Auto-generated method stub

		/*
		 * chatEditfield.click(); String background = randomAlphaNumeric(10);
		 * chatEditfield.sendKeys(background); sendButton.click(); (new
		 * TouchAction(driver)).tap(new PointOption().withCoordinates(298,
		 * 300)).perform(); RemoteWebElement element = (RemoteWebElement)
		 * driver.findElementByXPath(
		 * "//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[5]/XCUIElementTypeOther[1]/XCUIElementTypeOther"
		 * ); String elementID = element.getId();
		 * 
		 * HashMap<String, String> scrollObject = new HashMap<String, String>();
		 * scrollObject.put("element", elementID); // Only for ‘scroll in element’
		 * scrollObject.put("direction", "down"); driver.executeScript("mobile:scroll",
		 * scrollObject);
		 * 
		 * driver.findElementByXPath("//XCUIElementTypeStaticText[@name=\"" + background
		 * + "\"]").click();
		 * 
		 * Thread.sleep(3000);
		 * 
		 * driver.findElementByXPath(
		 * "//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[5]/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeImage")
		 * .click();
		 */

		return true;
	}

	public boolean VerifyChat_CMS_Sync() throws InterruptedException {
		// TODO Auto-generated method stub

		/*
		 * chatEditfield.click(); String background = randomAlphaNumeric(10);
		 * chatEditfield.sendKeys(background); sendButton.click(); // (new
		 * TouchAction(driver)).tap(298, 300).perform(); (new
		 * TouchAction(driver)).tap(new PointOption().withCoordinates(298,
		 * 300)).perform(); Thread.sleep(3000); RemoteWebElement element =
		 * (RemoteWebElement) driver.findElementByXPath(
		 * "//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[5]/XCUIElementTypeOther[1]/XCUIElementTypeOther"
		 * ); String elementID = element.getId(); HashMap<String, String> scrollObject =
		 * new HashMap<String, String>(); scrollObject.put("element", elementID); //
		 * Only for ‘scroll in element’ scrollObject.put("direction", "down");
		 * driver.executeScript("mobile:scroll", scrollObject);
		 * driver.executeScript("mobile:scroll", scrollObject); Thread.sleep(1000);
		 * boolean tt = driver.findElementByXPath("//XCUIElementTypeStaticText[@name=\""
		 * + background + "\"]").isEnabled();
		 */

		return true;
	}

	public static WebElement expandRootElement(WebElement element) {
		WebElement ele = (WebElement) ((JavascriptExecutor) driver2).executeScript("return arguments[0].shadowRoot",
				element);
		return ele;
	}

	public boolean Ads() throws InterruptedException {
		// TODO Auto-generated method stub
		
		int size = driver.findElementsByXPath(
				"//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[5]/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell")
				.size();
		System.out.println(size);
		if (size > 0) {
		
		
		MobileElement el1 = (MobileElement) driver.findElementById("Play Ad");
		el1.click();
		
		
		String text=driver.findElementById("label_video_timer").getText();
		System.out.println(text);
        Thread.sleep(5000);
        String text1=driver.findElementById("label_video_timer").getText();
        System.out.println(text1);
        
		MobileElement el2 = (MobileElement) driver.findElementById("Stop Ad");
		el2.click();
		
		  if(text.equalsIgnoreCase(text1))
	        {
			  System.out.println("Time stopped");
	        	return true;
	        }
	        else {
	        	System.out.println("Time not stopped");
	        	return false;
	        }
		
		
		}
		
		System.out.println("Chat not loaded");
		return false;
		
	}

	/*
	 * public boolean SpoilerfreeSync() throws MalformedURLException,
	 * InterruptedException { // TODO Auto-generated method stub
	 * 
	 * manageRooms();
	 * 
	 * MobileElement el11 = (MobileElement) driver.findElementById("One Program");
	 * el11.click(); MobileElement el12 = (MobileElement)
	 * driver.findElementById("Enter Room"); el12.click(); MobileElement el13 =
	 * (MobileElement) driver
	 * .findElementById("[0] dba595c6-afab-4f73-b22f-c7c0cb317ca9"); el13.click();
	 * MobileElement el14 = (MobileElement)
	 * driver.findElementById("textfield_chat_input"); el14.click();
	 * el14.sendKeys("helllllloooooo"); MobileElement el15 = (MobileElement)
	 * driver.findElementById("disclosure indicator"); el15.click();
	 * 
	 * int size = driver.findElementsByXPath(
	 * "//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther[5]/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell")
	 * .size(); String text = driver.findElementByXPath(
	 * "//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther[5]/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell["
	 * + size + "]/XCUIElementTypeStaticText[2]") .getText();
	 * System.out.println(text);
	 * 
	 * initialization1();
	 * 
	 * MobileElement e1l1 = (MobileElement)
	 * driver1.findElementByAccessibilityId("API Environment"); e1l1.click();
	 * MobileElement e1l2 = (MobileElement)
	 * driver1.findElementByXPath("//XCUIElementTypeButton[@name=\"PROD\"]");
	 * e1l2.click();
	 * 
	 * RemoteWebElement element = (RemoteWebElement) driver1.findElementByXPath(
	 * "//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]"
	 * ); String elementID = element.getId(); // System.out.println(elementID);
	 * HashMap<String, String> scrollObject = new HashMap<String, String>();
	 * scrollObject.put("element", elementID); // Only for ‘scroll in element’
	 * scrollObject.put("direction", "down"); driver1.executeScript("mobile:scroll",
	 * scrollObject);
	 * 
	 * MobileElement e1l3 = (MobileElement) driver1.findElementByXPath(
	 * "//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeTable/XCUIElementTypeCell[13]"
	 * ); e1l3.click(); MobileElement e1l4 = (MobileElement)
	 * driver1.findElementByXPath("//XCUIElementTypeButton[@name=\"Turner\"]");
	 * e1l4.click();
	 * 
	 * scrollObject.put("direction", "up"); driver1.executeScript("mobile:scroll",
	 * scrollObject);
	 * 
	 * MobileElement e1l11 = (MobileElement)
	 * driver1.findElementByAccessibilityId("One Program"); e1l11.click();
	 * MobileElement e1l12 = (MobileElement)
	 * driver1.findElementByAccessibilityId("Enter Room"); e1l12.click();
	 * 
	 * MobileElement e1l13 = (MobileElement) driver1
	 * .findElementByAccessibilityId("[0] dba595c6-afab-4f73-b22f-c7c0cb317ca9");
	 * e1l13.click();
	 * 
	 * MobileElement e1l15 = (MobileElement)
	 * driver1.findElementByAccessibilityId("helllllloooooo"); int size1 =
	 * driver1.findElementsByXPath(
	 * "//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther[5]/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell")
	 * .size(); String text1 = driver1.findElementByXPath(
	 * "//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther[5]/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell["
	 * + size1 + "]/XCUIElementTypeStaticText[2]") .getText();
	 * System.out.println(text1);
	 * 
	 * return true;
	 * 
	 * }
	 */

	public boolean EmptyChatDefaultView() throws InterruptedException {
		// TODO Auto-generated method stub

		MobileElement el1 = (MobileElement) driver.findElementById("prog: PubNub Chat Program Prod");
		el1.click();
		
		
		
		RemoteWebElement element = (RemoteWebElement) driver.findElementByXPath(
				"//XCUIElementTypeSheet[@name=\"Events\"]/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther[3]");
		String elementID = element.getId(); // System.out.println(elementID);
		HashMap<String, String> scrollObject = new HashMap<String, String>();
		scrollObject.put("element", elementID); // Only for ‘scroll in element’
		scrollObject.put("direction", "down");
		driver.executeScript("mobile:scroll", scrollObject);
		
		
		
		
		MobileElement el2 = (MobileElement) driver.findElementById("Strictly Dont Use this");
		el2.click();
		MobileElement el3 = (MobileElement) driver.findElementById("OK");
		el3.click();
		MobileElement el4 = (MobileElement) driver.findElementById("Video Layout");
		el4.click();
		MobileElement el5 = (MobileElement) driver.findElementById("Here is some body text");
		el5.click();

		return true;
	}

	public boolean KeyboardDismissOverride() {
		// TODO Auto-generated method stub
		boolean isKeyboardShown = false;
		int size = driver.findElementsByXPath(
				"//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[5]/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell")
				.size();
		System.out.println(size);
		if (size > 0) {
		MobileElement el1 = (MobileElement) driver.findElementById("textfield_chat_input");
		el1.click();
		el1.sendKeys("7");
		MobileElement e1l1 = (MobileElement) driver.findElementById("chat send button");
		e1l1.click();
			System.out.println("inside chats");
		isKeyboardShown = ((HasOnScreenKeyboard) driver).isKeyboardShown();
		}

		return isKeyboardShown;
	}

	public boolean RemoveMefromChatDisplayName() throws InterruptedException {
		// TODO Auto-generated method stub

		int size = driver.findElementsByXPath(
				"//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[5]/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell")
				.size();
		boolean flag = true;
		
		for (int i = 1; i <= 3; i++) {
			String text = driver.findElementByXPath(
					"//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[5]/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell["
							+ size + "]/XCUIElementTypeStaticText[" + i + "]")
					.getText();
			System.out.println(text);
			if (text.contains("[ME]")) {
				flag = false;
			}
		}

	

		return flag;
	}

	public boolean VerifyChatReactions() throws InterruptedException {
		// TODO Auto-generated method stub
		/*
		 * chatEditfield.click(); String background = randomAlphaNumeric(10);
		 * chatEditfield.sendKeys(background); sendButton.click(); // (new
		 * TouchAction(driver)).tap(298, 300).perform(); (new
		 * TouchAction(driver)).tap(new PointOption().withCoordinates(298,
		 * 300)).perform(); Thread.sleep(3000); RemoteWebElement element =
		 * (RemoteWebElement) driver.findElementByXPath(
		 * "//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[5]/XCUIElementTypeOther[1]/XCUIElementTypeOther"
		 * ); String elementID = element.getId(); HashMap<String, String> scrollObject =
		 * new HashMap<String, String>(); scrollObject.put("element", elementID); //
		 * Only for ‘scroll in element’ scrollObject.put("direction", "down");
		 * driver.executeScript("mobile:scroll", scrollObject);
		 * driver.executeScript("mobile:scroll", scrollObject); Thread.sleep(1000);
		 * boolean tt = driver.findElementByXPath("//XCUIElementTypeStaticText[@name=\""
		 * + background + "\"]").isEnabled();
		 */

		return true;
	}

	public boolean Profile_image() throws Exception {
		// TODO Auto-generated method stub

		MobileElement el1 = (MobileElement) driver.findElementById("Pick User Image");
		el1.click();
		MobileElement el2 = (MobileElement) driver.findElementById("Steve Ballmer");
		el2.click();
		MobileElement el3 = (MobileElement) driver.findElementById("textfield_chat_input");
		el3.click();
		el3.sendKeys("hhhhhh");
		Thread.sleep(3000);
		MobileElement el4 = (MobileElement) driver.findElementById("disclosure indicator");
		el4.click();

		// Thread.sleep(3000);
		driver.findElementsByXPath(
				"//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther[5]/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[10]/XCUIElementTypeOther/XCUIElementTypeImage");

		return true;
	}

	public boolean External_GIfs() throws InterruptedException {
		// TODO Auto-generated method stub

		int a = driver.findElementsByXPath("//XCUIElementTypeStaticText[@value=\"￼\"]").size();
		System.out.println(a);
		MobileElement el6 = (MobileElement) driver.findElementById("textfield_chat_input");
		el6.click();
		MobileElement el7 = (MobileElement) driver.findElementById("Next keyboard"); // el7.click();
		Thread.sleep(5000);
		Point p = (Point) driver.findElementById("Next keyboard").getCenter();

		TouchAction action = new TouchAction(driver);

		action.longPress(LongPressOptions.longPressOptions().withPosition(PointOption.point(p.x, p.y))
				.withDuration(Duration.ofMillis(3000))).release().perform();

		MobileElement el11 = (MobileElement) driver
				.findElementByXPath("//XCUIElementTypeCell[@name=\"Bitmoji, English\"]/XCUIElementTypeOther");
		el11.click();

		Thread.sleep(10000);
		MobileElement element = (MobileElement) driver.findElementByXPath(
				"//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[4]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther");

		int leftX = element.getLocation().getX() + 13;
		int rightX = leftX + element.getSize().getWidth();
		int middleX = (rightX + leftX) / 2;
		int upperY = element.getLocation().getY();
		int lowerY = upperY + element.getSize().getHeight();
		int middleY = (upperY + lowerY) / 2;
		middleY = middleY - 50;
		System.out.println(leftX + " " + rightX + " " + middleX + " " + upperY + " " + lowerY + " " + middleY + " ");

		(new TouchAction(driver)).tap(new PointOption().withCoordinates(leftX, middleY)).perform();

		Thread.sleep(3000);
		MobileElement textfield_chat_input = (MobileElement) driver
				.findElementById("textfield_chat_input");
		textfield_chat_input.click();

		MobileElement Paste = (MobileElement) driver.findElementById("Paste");
		Paste.click();
		MobileElement el1 = (MobileElement) driver.findElementById("chat send button");
		el1.click();

		(new TouchAction(driver)).tap(new PointOption().withCoordinates(leftX, lowerY)).perform();
		Thread.sleep(2000);
		(new TouchAction(driver)).tap(new PointOption().withCoordinates(leftX, lowerY)).perform();

		int b = driver.findElementsByXPath("//XCUIElementTypeStaticText[@value=\"￼\"]").size();

		System.out.println(b);
		if ((b - a) == 1) {
			return true;
		}
		return false;

	}

	public boolean WidgetOveridee() throws InterruptedException, IOException, JSONException {
		// TODO Auto-generated method stub

		TextQuizApi.authcheck();

		// System.out.println("Search");

		Thread.sleep(5000);
		driver.findElementByXPath(
				"//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[4]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeButton[2]")
				.click();

		Thread.sleep(15000);
		System.out.println("TAKE SCREENSHOT");
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(file, new File("QIOS1.jpg"));

		Thread.sleep(14000);

		TextQuizApi.authcheck();

		Thread.sleep(3000);
		driver.findElementByXPath(
				"//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[4]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeButton[2]")
				.click();

		Thread.sleep(15000);
		System.out.println("TAKE SCREENSHOT");
		File file2 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(file2, new File("QIOS2.jpg"));

		return !(FileUtils.contentEquals(file, file2));

	}

	/*
	 * public boolean UnreadMessageCount() throws InterruptedException,
	 * MalformedURLException { // TODO Auto-generated method stub
	 * 
	 * MobileElement el1 = (MobileElement)
	 * driver.findElementById("API Environment"); el1.click(); MobileElement el2 =
	 * (MobileElement)
	 * driver.findElementByXPath("//XCUIElementTypeButton[@name=\"PROD\"]");
	 * el2.click(); MobileElement el3 = (MobileElement) driver.findElementByXPath(
	 * "//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeTable/XCUIElementTypeCell[13]"
	 * ); el3.click(); MobileElement el4 = (MobileElement)
	 * driver.findElementByXPath("//XCUIElementTypeButton[@name=\"Turner\"]");
	 * el4.click();
	 * 
	 * MobileElement e1l1 = (MobileElement) driver.findElementByXPath(
	 * "//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeTable/XCUIElementTypeCell[7]"
	 * ); e1l1.click(); if
	 * (!driver.findElementById("dba595c6-afab-4f73-b22f-c7c0cb317ca9").isDisplayed(
	 * )) { System.out.println("manage rooms"); manageRooms(); }
	 * System.out.println("no manage rooms"); // cancel MobileElement ecl1 =
	 * (MobileElement) driver.findElementByXPath(
	 * "//XCUIElementTypeSheet[@name=\"The following are *Joined* Rooms\"]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther"
	 * ); ecl1.click();
	 * 
	 * MobileElement el11 = (MobileElement) driver.findElementById("One Program");
	 * el11.click(); MobileElement el12 = (MobileElement)
	 * driver.findElementById("Enter Room"); el12.click(); MobileElement el13 =
	 * (MobileElement) driver
	 * .findElementById("[0] dba595c6-afab-4f73-b22f-c7c0cb317ca9"); el13.click();
	 * MobileElement el14 = (MobileElement)
	 * driver.findElementById("textfield_chat_input"); el14.click();
	 * el14.sendKeys("helllllloooooo"); MobileElement el15 = (MobileElement)
	 * driver.findElementById("disclosure indicator"); el15.click();
	 * 
	 * initialization1();
	 * 
	 * MobileElement e11l1 = (MobileElement)
	 * driver1.findElementByAccessibilityId("API Environment"); e11l1.click();
	 * MobileElement e1l2 = (MobileElement)
	 * driver1.findElementByXPath("//XCUIElementTypeButton[@name=\"PROD\"]");
	 * e1l2.click();
	 * 
	 * RemoteWebElement element = (RemoteWebElement) driver1.findElementByXPath(
	 * "//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]"
	 * ); String elementID = element.getId(); // System.out.println(elementID);
	 * HashMap<String, String> scrollObject = new HashMap<String, String>();
	 * scrollObject.put("element", elementID); // Only for ‘scroll in element’
	 * scrollObject.put("direction", "down"); driver1.executeScript("mobile:scroll",
	 * scrollObject);
	 * 
	 * MobileElement e1l3 = (MobileElement) driver1.findElementByXPath(
	 * "//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeTable/XCUIElementTypeCell[13]"
	 * ); e1l3.click(); MobileElement e1l4 = (MobileElement)
	 * driver1.findElementByXPath("//XCUIElementTypeButton[@name=\"Turner\"]");
	 * e1l4.click();
	 * 
	 * scrollObject.put("direction", "up"); driver1.executeScript("mobile:scroll",
	 * scrollObject);
	 * 
	 * MobileElement e1l11 = (MobileElement)
	 * driver1.findElementByAccessibilityId("One Program"); e1l11.click();
	 * MobileElement e1l12 = (MobileElement)
	 * driver1.findElementByAccessibilityId("Enter Room"); e1l12.click();
	 * 
	 * String text = driver1.
	 * findElementByAccessibilityId("[1] f05ee348-b8e5-4107-8019-c66fad7054a8").
	 * getText(); System.out.println(text); return true; }
	 */

	public boolean External_GIfs_Giffy() throws InterruptedException {
		// TODO Auto-generated method stub

		int a = driver.findElementsByXPath("//XCUIElementTypeStaticText[@value=\"￼\"]").size();
		System.out.println(a);
		MobileElement el6 = (MobileElement) driver.findElementById("textfield_chat_input");
		el6.click();
		Thread.sleep(3000);
		MobileElement el7 = (MobileElement) driver.findElementById("Next keyboard"); // el7.click();
		Thread.sleep(5000);
		Point p = (Point) driver.findElementById("Next keyboard").getCenter();

		TouchAction action = new TouchAction(driver);

		action.longPress(LongPressOptions.longPressOptions().withPosition(PointOption.point(p.x, p.y))
				.withDuration(Duration.ofMillis(3000))).release().perform();

		MobileElement GIPHY = (MobileElement) driver.findElementById("GIPHY");
		GIPHY.click();

		Thread.sleep(10000);
		MobileElement element = (MobileElement) driver.findElementByXPath(
				"//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[4]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther");

		int leftX = element.getLocation().getX() + 13;
		int rightX = leftX + element.getSize().getWidth();
		int middleX = (rightX + leftX) / 2;
		int upperY = element.getLocation().getY();
		int lowerY = upperY + element.getSize().getHeight();
		int middleY = (upperY + lowerY) / 2;
		middleY = middleY - 50;
		System.out.println(leftX + " " + rightX + " " + middleX + " " + upperY + " " + lowerY + " " + middleY + " ");

		(new TouchAction(driver)).tap(new PointOption().withCoordinates(leftX, middleY)).perform();

		Thread.sleep(3000);
		MobileElement textfield_chat_input = (MobileElement) driver
				.findElementById("textfield_chat_input");
		textfield_chat_input.click();

		MobileElement Paste = (MobileElement) driver.findElementById("Paste");
		Paste.click();
		MobileElement el1 = (MobileElement) driver.findElementById("chat send button");
		el1.click();

		(new TouchAction(driver)).tap(new PointOption().withCoordinates(leftX, lowerY)).perform();

		int b = driver.findElementsByXPath("//XCUIElementTypeStaticText[@value=\"￼\"]").size();
		System.out.println(b);
		if ((b - a) == 1) {
			return true;
		}
		return false;

	}

	public boolean Turner_Chat_Reaction() throws InterruptedException {
		// TODO Auto-generated method stub

		int a = driver.findElementsByXPath("//XCUIElementTypeStaticText[@name=\"reactionCountLabel\"]").size();

		System.out.println(a);
		String background = randomAlphaNumeric(10);

		MobileElement textfield_chat_input = (MobileElement) driver
				.findElementById("textfield_chat_input");
		textfield_chat_input.click();
		textfield_chat_input.sendKeys(background);
		MobileElement el1 = (MobileElement) driver.findElementById("chat send button");
		el1.click();
		RemoteWebElement element = (RemoteWebElement) driver.findElementByXPath(
				"//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[5]/XCUIElementTypeOther[1]/XCUIElementTypeOther");
		String elementID = element.getId();
		HashMap<String, String> scrollObject = new HashMap<String, String>();
		scrollObject.put("element", elementID);

		scrollObject.put("direction", "down");
		driver.executeScript("mobile:scroll", scrollObject);
		driver.executeScript("mobile:scroll", scrollObject);
		driver.executeScript("mobile:scroll", scrollObject);
		Point p1 = driver.findElementById(background).getCenter();

		int x1 = p1.getX();
		int y1 = p1.getY();

		(new TouchAction(driver)).tap(new PointOption().withCoordinates(x1, y1)).perform();

		/*
		 * int size=driver.findElementsByXPath(
		 * "//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther[5]/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell"
		 * ).size(); size=size-1; System.out.println(size); String
		 * text=driver.findElementByXPath(
		 * "//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther[5]/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell["
		 * +size+"]/XCUIElementTypeStaticText[1]").getText(); System.out.println(text);
		 * 
		 * RemoteWebElement element = (RemoteWebElement) driver.findElementByXPath(
		 * "//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[5]/XCUIElementTypeOther[1]/XCUIElementTypeOther"
		 * ); String elementID = element.getId(); HashMap<String, String> scrollObject =
		 * new HashMap<String, String>(); scrollObject.put("element", elementID);
		 * 
		 * scrollObject.put("direction", "down"); driver.executeScript("mobile:scroll",
		 * scrollObject); driver.executeScript("mobile:scroll", scrollObject);
		 * driver.executeScript("mobile:scroll", scrollObject); Point
		 * a1=driver.findElementByXPath(
		 * "//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther[5]/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell["
		 * +size+"]/XCUIElementTypeStaticText[1]").getCenter();
		 * 
		 * int x=a1.getX(); int y=a1.getY(); System.out.println(x+" "+y); (new
		 * TouchAction(driver)).tap(new PointOption().withCoordinates(x,y)).perform();
		 * 
		 * Thread.sleep(3000); Point a2=driver.findElementByXPath(
		 * "//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther[5]/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell["
		 * +size+"]/XCUIElementTypeStaticText[1]").getCenter();
		 * 
		 * int x1=a2.getX(); int y1=a2.getY(); System.out.println(x1+" "+y1); (new
		 * TouchAction(driver)).tap(new PointOption().withCoordinates(x1,y1)).perform();
		 */

		driver.executeScript("mobile:scroll", scrollObject);
		driver.executeScript("mobile:scroll", scrollObject);
		driver.executeScript("mobile:scroll", scrollObject);
		Point p = driver.findElementById(background).getCenter();

		int x = p.getX();
		int y = p.getY();

		(new TouchAction(driver)).tap(new PointOption().withCoordinates(x, y)).perform();

		MobileElement el11 = (MobileElement) driver.findElementById("Reactions Panel Opened");
		el11.click();

		int b = driver.findElementsByXPath("//XCUIElementTypeStaticText[@name=\"reactionCountLabel\"]").size();
		System.out.println(b);
		if ((b - a) == 1) {
			return true;
		}
		return false;

	}

	public boolean Turner_SetUsername_Reaction() {
		// TODO Auto-generated method stub

		String background = randomAlphaNumeric(10);
		System.out.println(background);
		MobileElement el1 = (MobileElement) driver.findElementById("Set Username");
		el1.click();
		MobileElement el2 = (MobileElement) driver.findElementByXPath(
				"//XCUIElementTypeAlert[@name=\"Change User Display Name\"]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther");
		el2.sendKeys(background);
		MobileElement el3 = (MobileElement) driver.findElementById("Submit");
		el3.click();
		MobileElement el4 = (MobileElement) driver.findElementById("<- Event: PubNub Chat Program Prod");
		el4.click();
		MobileElement e1l3 = (MobileElement) driver.findElementById("Video Layout");
		e1l3.click();
		MobileElement el6 = (MobileElement) driver.findElementById(background);
		el6.click();

		return true;
	}

}

package com.crm.qa.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import org.openqa.selenium.remote.DesiredCapabilities;



public class CapabilityReader {

	public CapabilityReader() {
		
		
	}
	
	public DesiredCapabilities getCapabilities()
	{
		DesiredCapabilities caps= new DesiredCapabilities();
		
		BufferedReader reader;
		try {
			reader= new BufferedReader(new FileReader(ConfigReader.getCABS_FILEPATH()));   
			String line=reader.readLine();
			while(line!=null)
			{
				//System.out.println(line);
				String[] cap=line.split("="); 
				System.out.println(cap[0]+"  "+cap[1]);
				//System.out.println(cap);
				caps.setCapability(cap[0], cap[1]);
				line=reader.readLine();
			}
			reader.close();
		}
		catch(IOException e)
		{
			System.out.println(e);
		}
		return caps;
	}
	
	

}

package com.crm.qa.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigReader {
	static Properties prop =null;
	static {
		 prop=new Properties();
		InputStream input=null;
		try {
			input=new FileInputStream("config.properties");
			prop.load(input);
			
		}
		catch(IOException e)
		{
			System.out.println(e);
		}
		finally {
			if(input!=null)
			{
				try {
					input.close();
				}
				catch(IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public static String getPlatFormName()
	{
		return prop.getProperty("PLATFORM_NAME");
	}
	public static String getCABS_FILEPATH()
	{
		return prop.getProperty("CABS_FILEPATH");
	}
	public static String getAPPIUM_SERVER_URL()
	{
		return prop.getProperty("APPIUM_SERVER_URL");
	}
	public static void main(String args[]) {
		System.out.println(ConfigReader.getPlatFormName());
	}

}

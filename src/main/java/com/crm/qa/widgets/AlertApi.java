package com.crm.qa.widgets;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.crm.qa.base.TestBase;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import android.content.SyncStatusObserver;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class AlertApi extends TestBase{
	static Response response;
	
	static JsonPath jsonPathEvaluator;
	static boolean flag=false;
	static String sche;
		
	public static void authcheck() throws IOException, InterruptedException, JSONException{
	String bea = "DblPy3BVfqj_aatq6N5kFpNcH08LKObqEbnlLWc-3NlFInKTfsVxEg";
	
	try{
    	RequestSpecification request = RestAssured.given();
    	request.header("Content-Type","application/json");
    	request.header("Authorization","Bearer "+bea);
    	/*request.header("Referer","https://producer-staging.livelikecdn.com/apps/l3euw77STbgCVQn59AQZudZ163qO2EhzWVOKBfpu/programs/c3505f34-85e3-4301-b2aa-b1d463b83608");
    	request.header("Origin","https://producer-staging.livelikecdn.com");
    	request.header("User-Agent","Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36");
    	request.header("DNT","1");*/
    	
    	
    	
    	//System.out.println(bea);
    	//Read the json data file from test_data folder
    	String data = ""; 
    	data = new String("{\"timeout\":\"P0DT00H00M20S\",\"title\":\"dfhdddhf\",\"text\":\"dfhdfhdffdh\",\"image_url\":\"https://cf-blast-storage.livelikecdn.com/assets/8f311d09-cd29-447d-8bba-c24c719c9b99.jpg\",\"link_url\":\"https://producer.livelikecdn.com/apps/It40FlkcwAxpIgdMQxlRlQODkl4TbfkWdYo8tNAb/programs/643673fa-ca7c-4dd2-b264-418390ff3e19\",\"link_label\":\"Hello\",\"program_date_time\":null,\"program_id\":\"6834f1fd-f24d-4538-ba51-63544f9d78eb\"}"); 
        request.body(data);
        response = request.post("https://cf-blast.livelikecdn.com/api/v1/alerts/");
    	
    	
    	    //Reader header of a give name. In this line we will get
           //Header named Content-Type
    	   String contentType = response.header("Content-Type");
    	   //System.out.println("Content-Type value: " + contentType);
		System.out.println(response.asString());
		System.out.println("////////////////////////////////////////////////////////");
		int statusCode = response.getStatusCode();
		// Assert that correct status code is returned.
		Assert.assertEquals(statusCode /*actual value*/, 201 /*expected value*/, "Status code returned:");
		
		
		
		//put method
		// First get the JsonPath object instance from the Response interface
		 jsonPathEvaluator = response.jsonPath();
		// System.out.println("URl:" + jsonPathEvaluator.get("schedule_url"));
		 sche=jsonPathEvaluator.get("schedule_url");
		 Response response1 = request.put(sche);
		 System.out.println(response1.asString());
		 System.out.println("////////////////////////////////////////////////////////");
		 int statusCode1 = response1.getStatusCode();
		 System.out.println("PUT status "+statusCode1);
		//Assert that correct status code is returned.
		Assert.assertEquals(statusCode1 /*actual value*/, 200 /*expected value*/, "Status code returned:");
		
		
	}
	

	catch(AssertionError e)
	{
		
		System.out.println("Exception is:"+e);
		//test.log(LogStatus.FAIL, "POST");
	}
	
	}
	
	public static void authcheck1() throws IOException, InterruptedException, JSONException{
		String bea = "DblPy3BVfqj_aatq6N5kFpNcH08LKObqEbnlLWc-3NlFInKTfsVxEg";
		RequestSpecification request1 = RestAssured.given();
    	request1.header("Content-Type","application/json");
    	request1.header("Authorization","Bearer "+bea);
    
		String sche=jsonPathEvaluator.get("schedule_url");
        String sche1 = sche.replace("schedule/", ""); 
        //System.out.println(sche1);
        Response response2 = request1.get(sche1);
        JsonPath jsonPathEvaluator1 = response2.jsonPath();
        System.out.println(response2.asString());
        System.out.println("////////////////////////////////////////////////////////");
	    String widget_status = jsonPathEvaluator1.get("status");
	   //Validate the response
	    System.out.println(widget_status);
	   
	    
	    System.out.println("////////////////////////////////////////////////////////");
	    Assert.assertEquals(widget_status, "published", "Published received in the Response");
	    
		/*
		 * Thread.sleep(13000); Response response3 = request1.get(sche1); JsonPath
		 * jsonPathEvaluator2 = response3.jsonPath();
		 * System.out.println(response3.asString());
		 * System.out.println("////////////////////////////////////////////////////////"
		 * );
		 */
        	
        	
	    }
	
	
	
}

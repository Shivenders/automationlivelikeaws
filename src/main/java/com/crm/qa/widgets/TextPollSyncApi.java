package com.crm.qa.widgets;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.crm.qa.base.TestBase;

import android.content.SyncStatusObserver;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class TextPollSyncApi extends TestBase{
	static Response response;
	
	static JsonPath jsonPathEvaluator;
	static String sche;

	
		
	public static void authcheck() throws IOException, InterruptedException, JSONException{
	String bea = "DblPy3BVfqj_aatq6N5kFpNcH08LKObqEbnlLWc-3NlFInKTfsVxEg";
	
	try{
    	RequestSpecification request = RestAssured.given();
    	request.header("Content-Type","application/json");
    	request.header("Authorization","Bearer "+bea);
    	
    	
    	
    	
    	
    	String data = ""; 
    	data = new String("{\"timeout\":\"P0DT00H00M20S\",\"question\":\"burger 1\",\"options\":[{\"description\":\"burger 2\"},{\"description\":\"UEFA EUROPA LEAGUE LOGO\"}],\"program_date_time\":\"2019-12-02T01:57:55.371Z\",\"program_id\":\"643673fa-ca7c-4dd2-b264-418390ff3e19\"}"); 
        request.body(data);
        response = request.post("https://cf-blast.livelikecdn.com/api/v1/text-polls/");
    	
    	
    	   
    	   String contentType = response.header("Content-Type");
    	   //System.out.println("Content-Type value: " + contentType);
		System.out.println(response.asString());
		System.out.println("////////////////////////////////////////////////////////");
		int statusCode = response.getStatusCode();
		// Assert that correct status code is returned.
		Assert.assertEquals(statusCode /*actual value*/, 201 /*expected value*/, "Status code returned:");
  
		if(statusCode==201)
		{
			testInfo.log(Status.INFO, "TextPoll Widget: TextPoll API POST method successfully returned 201 status code.");
		}
		//put method
		// First get the JsonPath object instance from the Response interface
		 jsonPathEvaluator = response.jsonPath();
		// System.out.println("URl:" + jsonPathEvaluator.get("schedule_url"));
		 sche=jsonPathEvaluator.get("schedule_url");
		 Response response1 = request.put(sche);
		 System.out.println(response1.asString());
		 System.out.println("////////////////////////////////////////////////////////");
		 int statusCode1 = response1.getStatusCode();
		//Assert that correct status code is returned.
		Assert.assertEquals(statusCode1 /*actual value*/, 200 /*expected value*/, "Status code returned:");
		if(statusCode1==200)
		{
			testInfo.log(Status.INFO, "TextPoll Widget: TextPoll API PUT method successfully returned 200 status code.");
		}
		
	}
	

	catch(AssertionError e)
	{
		
		System.out.println("Exception is:"+e);
		//test.log(LogStatus.FAIL, "POST");
	}
	
	}
	
	
	
	
	
}

package com.crm.qa.widgetsAndroid;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.crm.qa.base.TestBase;

import android.content.SyncStatusObserver;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class ImagePredictionApiAndroid extends TestBase{
	static Response response;
	
	
	static JsonPath jsonPathEvaluator;
	static String sche;


	
		
	public static void authcheck() throws IOException, InterruptedException, JSONException{
	String bea = "DblPy3BVfqj_aatq6N5kFpNcH08LKObqEbnlLWc-3NlFInKTfsVxEg";
	
	try{
    	RequestSpecification request = RestAssured.given();
    	request.header("Content-Type","application/json");
    	request.header("Authorization","Bearer "+bea);
    	/*request.header("Referer","https://producer-staging.livelikecdn.com/apps/l3euw77STbgCVQn59AQZudZ163qO2EhzWVOKBfpu/programs/c3505f34-85e3-4301-b2aa-b1d463b83608");
    	request.header("Origin","https://producer-staging.livelikecdn.com");
    	request.header("User-Agent","Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36");
    	request.header("DNT","1");*/
    	
    	imagepredictionjason="{\"timeout\":\"P0DT00H00M20S\",\"question\":\"Hello What do you need\",\"options\":[{\"description\":\"Justin Gatlin\",\"image_url\":\"https://cf-blast-storage.livelikecdn.com/assets/f435f54f-3abc-4953-a6c8-654afad402a1.jpg\"},{\"description\":\"Usain Bolt\",\"image_url\":\"https://cf-blast-storage.livelikecdn.com/assets/8f311d09-cd29-447d-8bba-c24c719c9b99.jpg\"}],\"confirmation_message\":\"Usain Bolt\",\"program_date_time\":null,\"program_id\":\""+prop.getProperty("programId")+"\"}";
    	
    	//System.out.println(bea);
    	//Read the json data file from test_data folder
    	String data = ""; 
        data = new String(Files.readAllBytes(Paths.get(System.getProperty("user.dir") +"/JsonFile/imageprediction.json"))); 
        request.body(imagepredictionjason);
        response = request.post("https://cf-blast.livelikecdn.com/api/v1/image-predictions/");
    	
    	
    	    //Reader header of a give name. In this line we will get
           //Header named Content-Type
    	   String contentType = response.header("Content-Type");
    	   //System.out.println("Content-Type value: " + contentType);
		System.out.println(response.asString());
		System.out.println("////////////////////////////////////////////////////////");
		int statusCode = response.getStatusCode();
		// Assert that correct status code is returned.
		Assert.assertEquals(statusCode /*actual value*/, 201 /*expected value*/, "Status code returned:");
		if(statusCode==201)
		{
			testInfo.log(Status.INFO, "ImagePrediction Widget: ImagePrediction API POST method successfully returned 201 status code.");
		}
		
		//put method
		// First get the JsonPath object instance from the Response interface
		 jsonPathEvaluator = response.jsonPath();
		// System.out.println("URl:" + jsonPathEvaluator.get("schedule_url"));
		 sche=jsonPathEvaluator.get("schedule_url");
		 Response response1 = request.put(sche);
		 System.out.println(response1.asString());
		 System.out.println("////////////////////////////////////////////////////////");
		 int statusCode1 = response1.getStatusCode();
		//Assert that correct status code is returned.
		Assert.assertEquals(statusCode1 /*actual value*/, 200 /*expected value*/, "Status code returned:");
		if(statusCode1==200)
		{
			testInfo.log(Status.INFO, "ImagePrediction Widget: ImagePrediction API PUT method successfully returned 200 status code.");
		}
		
	}
	

	catch(AssertionError e)
	{
		
		System.out.println("Exception is:"+e);
		//test.log(LogStatus.FAIL, "POST");
	}
	
	}
	
	public static void authcheck1() throws IOException, InterruptedException, JSONException{
		String bea = "DblPy3BVfqj_aatq6N5kFpNcH08LKObqEbnlLWc-3NlFInKTfsVxEg";
		RequestSpecification request1 = RestAssured.given();
    	request1.header("Content-Type","application/json");
    	request1.header("Authorization","Bearer "+bea);
    
		String sche=jsonPathEvaluator.get("schedule_url");
        String sche1 = sche.replace("schedule/", ""); 
        //System.out.println(sche1);
        Response response2 = request1.get(sche1);
        JsonPath jsonPathEvaluator1 = response2.jsonPath();
        System.out.println(response2.asString());
        System.out.println("////////////////////////////////////////////////////////");
	    String widget_status = jsonPathEvaluator1.get("status");
	   //Validate the response
	    System.out.println(widget_status);
	    System.out.println("////////////////////////////////////////////////////////");
	    Assert.assertEquals(widget_status, "published", "Published received in the Response");
	    if(widget_status.equals("published"))
			testInfo.log(Status.INFO, "ImagePrediction Widget Status : "+widget_status);
	    Thread.sleep(13000);
	    Response response3 = request1.get(sche1);
        JsonPath jsonPathEvaluator2 = response3.jsonPath();
       System.out.println(response3.asString());
       System.out.println("////////////////////////////////////////////////////////");
        List<String> values = jsonPathEvaluator2.getList("options.vote_count");
        
        for(int i=0;i<values.size();i++)
        {
        	
        	
        	 String a = String.valueOf(values.get(i));
        	 
        	if(Integer.parseInt(a)>0)
        	{
        		System.out.println(a);
        		testInfo.log(Status.INFO, " Expected vote count > 0 || Actual vote count : "+a);
        		break;
        	}
        }
	    }

	public static void authcheck2() throws IOException, InterruptedException, JSONException{
		// TODO Auto-generated method stub
		
		
		
			
	    	
	    	
	    	
	    	//System.out.println("Ye hi hai "+response.asString());
	    	
	    	String usain="";
	    	String gat="";
	    	String furl="";
	    	List<HashMap<String, Object>> columns = jsonPathEvaluator.getList("follow_ups");
	    	furl=(String) columns.get(0).get("id");
	    	for (HashMap<String, Object> singleColumn : columns) {
	    	    List<HashMap<String, Object>> options = (List<HashMap<String, Object>>) singleColumn.get("options");
	    	    gat=(String) options.get(0).get("id");
	    	    usain=(String) options.get(1).get("id");
	    	}
			System.out.println("furl"+furl);
			System.out.println("usain"+usain);
			System.out.println("gat"+gat);
			
		
		  String bea = "DblPy3BVfqj_aatq6N5kFpNcH08LKObqEbnlLWc-3NlFInKTfsVxEg";
		  RequestSpecification request2 = RestAssured.given();
		  request2.header("Content-Type","application/json");
		  request2.header("Authorization","Bearer "+bea);
		  
		  
		  
		  String data = ""; data = new String("{\"is_correct\":true}");
		  request2.body(data); 
		  Response response11;
		  
		  response11 = request2.patch("https://cf-blast.livelikecdn.com/api/v1/image-prediction-options/"+usain+"/");
		  
		  System.out.println(response11.asString());
		  
		  
		  
		  RequestSpecification request3 = RestAssured.given();
		  request3.header("Content-Type","application/json");
		  request3.header("Authorization","Bearer "+bea);
		  
		  
		  
		  String data1 = ""; data1 = new String("{\"is_correct\":false}");
		  request3.body(data1); 
		  Response response12;
		  
		  response12 = request3.patch("https://cf-blast.livelikecdn.com/api/v1/image-prediction-options/"+gat+"/");
		  
		  System.out.println(response12.asString());
		  
		  RequestSpecification request4 = RestAssured.given();
		  request4.header("Content-Type","application/json");
		  request4.header("Authorization","Bearer "+bea);
		  
		  
		  
		  String data2 = ""; data2 = new String("{\"timeout\":\"P0DT00H00M20S\",\"question\":\"Hello What do you need\",\"options\":[{\"id\":\""+gat+"\",\"url\":\"https://cf-blast.livelikecdn.com/api/v1/image-prediction-options/"+gat+"/\",\"description\":\"Justin Gatlin\",\"is_correct\":false,\"vote_count\":0,\"vote_url\":\"https://cf-blast.livelikecdn.com/api/v1/image-prediction-options/"+gat+"/votes/\",\"image_url\":\"https://cf-blast-storage.livelikecdn.com/assets/f435f54f-3abc-4953-a6c8-654afad402a1.jpg\",\"translations\":{},\"translatable_fields\":[\"description\"]},{\"id\":\""+usain+"\",\"url\":\"https://cf-blast.livelikecdn.com/api/v1/image-prediction-options/"+usain+"/\",\"description\":\"Usain Bolt\",\"is_correct\":true,\"vote_count\":1,\"vote_url\":\"https://cf-blast.livelikecdn.com/api/v1/image-prediction-options/"+usain+"/votes/\",\"image_url\":\"https://cf-blast-storage.livelikecdn.com/assets/8f311d09-cd29-447d-8bba-c24c719c9b99.jpg\",\"translations\":{},\"translatable_fields\":[\"description\"]}],\"program_date_time\":\"2019-10-02T15:55:26.626Z\"}");
		  request4.body(data2); 
		  Response response14;
		  
		  response14 = request4.patch("https://cf-blast.livelikecdn.com/api/v1/image-prediction-follow-ups/"+furl+"/");
		  System.out.println(response14.asString());
		  
		  jsonPathEvaluator = response14.jsonPath();
			// System.out.println("URl:" + jsonPathEvaluator.get("schedule_url"));
			 sche=jsonPathEvaluator.get("schedule_url");
			 Response response15 = request4.put(sche);
			 System.out.println("final"+response15.asString());
			 
	
        }
	
		
		
	}
	


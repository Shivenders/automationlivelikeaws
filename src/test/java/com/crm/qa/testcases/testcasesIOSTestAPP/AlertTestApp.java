package com.crm.qa.testcases.testcasesIOSTestAPP;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.json.JSONException;
import org.openqa.selenium.OutputType;
//import org.json.JSONException;
import org.openqa.selenium.Point;
import org.openqa.selenium.ScreenOrientation;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.crm.qa.base.TestBase;

import com.crm.qa.widgets.TextPollApi;
import com.crm.qa.widgets.AlertApi;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.appmanagement.ApplicationState;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.ShakesDevice;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;



public class AlertTestApp  extends TestBase{
	@SuppressWarnings("rawtypes")
	//public static AppiumDriver driver;
	
	
	
	
	
	

	
	@Test
	public static void Alert_Widget_Test_Potrait() throws InterruptedException, IOException, JSONException{

		
		//initialization();
		driver.rotate(ScreenOrientation.PORTRAIT);
		iostestapp();
		System.out.println("Im in Alert_Widget_Test_Potrait");
		
		
		if (driver.findElementsByXPath(
				"//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[5]/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell")
				.size() > 0) {
		AlertApi.authcheck();
		
		driver.findElementByXPath("//XCUIElementTypeStaticText[@name=\"Hello\"]").click();
		if(driver.findElementsById("Video").size()<=0)
		{
			Assert.assertTrue(true);
		}
		else
		{
			Assert.assertTrue(false);
		}
		AlertApi.authcheck1();
		}
		else {
			System.out.println("Chats not loaded");
			Assert.assertTrue(false);
		}
		
		
	}
	
	
	
	  @Test public static void Alert_Widget_Test_Landscape() throws
	  InterruptedException, IOException, JSONException{
	  
	 
	  //initialization();  
		  iostestapp();
	  Thread.sleep(1000);
	  driver.rotate(ScreenOrientation.LANDSCAPE);
	  System.out.println("Im in Alert_Widget_Test_Landscape");
	  
	  
	  
	  if (driver.findElementsByXPath(
			  "//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[4]/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell")
				.size() > 0) {
		AlertApi.authcheck();
		
		
		
		driver.findElementByXPath("//XCUIElementTypeStaticText[@name=\"Hello\"]").click();
		if(driver.findElementsById("Video").size()<=0)
		{
			Assert.assertTrue(true);
		}
		else
		{
			Assert.assertTrue(false);
		}
		AlertApi.authcheck1();
		driver.rotate(ScreenOrientation.PORTRAIT);
		}
		
		else {
			System.out.println("Chats not loaded");
			Assert.assertTrue(false);
		}
	  }
	  
	
	  
	 
	 

}

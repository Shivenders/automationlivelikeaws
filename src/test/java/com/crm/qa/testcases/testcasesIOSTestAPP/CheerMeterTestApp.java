package com.crm.qa.testcases.testcasesIOSTestAPP;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
//import java.time.Duration;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.json.JSONException;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
//import org.json.JSONException;
import org.openqa.selenium.Point;
import org.openqa.selenium.ScreenOrientation;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.crm.qa.base.TestBase;

import com.crm.qa.widgets.CheerMeterApi;
import com.crm.qa.widgets.TextPollApi;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.MultiTouchAction;
import io.appium.java_client.TouchAction;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.ShakesDevice;
import io.appium.java_client.touch.TapOptions;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.ElementOption;
import io.appium.java_client.touch.offset.PointOption;

public class CheerMeterTestApp extends TestBase {
	@SuppressWarnings("rawtypes")
	
	
	@Test
	public static void CheerMeter_Widget_Test_Potrait() throws InterruptedException, IOException, JSONException{


		
		//initialization();
		driver.rotate(ScreenOrientation.PORTRAIT);
		iostestapp();
		
		System.out.println("Im in CheerMeter_Widget_Test_Potrait");
		
		if (driver.findElementsByXPath(
				"//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[5]/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell")
				.size() > 0) {
		    CheerMeterApi.authcheck();
		
            driver.findElementByXPath("//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[4]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeImage[2]").click();
			System.out.println("clicking now!!!!!!!");
			

			TouchAction action = new TouchAction(driver);
			
			Point p= driver.findElementByXPath("//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[4]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeImage[2]").getCenter();
			int x=p.getX();
			int y=p.getY();
			
			for(int i=0;i<=15;i++)
			{
				
				
				
				
				
				(new TouchAction(driver)).tap(new PointOption().withCoordinates(x, y)).perform();
				
			
			}
		
		    
		
		    CheerMeterApi.authcheck1();
		
		}
		else {
			System.out.println("Chats not loaded");
			Assert.assertTrue(false);
		}
		
		
	}

	@Test
	public static void CheerMeter_Widget_Test_Landscape() throws InterruptedException, IOException, JSONException {

		
		//initialization();
		iostestapp();
		  Thread.sleep(1000);
		  driver.rotate(ScreenOrientation.LANDSCAPE);

		

		CheerMeterApi.authcheck();

		// System.out.println("Display to hai");
		driver.findElementByXPath(
				"//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeImage[2]")
				.click();

		

		System.out.println("clicking now!!!!!!!");

		TouchAction action = new TouchAction(driver);

		// WebElement ele =
		// driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[4]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeImage"));

		for (int i = 0; i <= 15; i++) {
			// action.tap(new TapOptions().withElement(new
			// ElementOption().withElement(ele))).perform();

			(new TouchAction(driver)).tap(new PointOption().withCoordinates(126, 110)).perform();

		}

		Thread.sleep(2000);

		CheerMeterApi.authcheck1();

		// //XCUIElementTypeApplication[@name="LiveLike
		// Demo"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[4]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeImage

		

		
		// Thread.sleep(5000);

	}

}

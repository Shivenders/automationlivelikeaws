package com.crm.qa.testcases.testcasesIOSTestAPP;

import java.io.IOException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.openqa.selenium.ScreenOrientation;
import org.openqa.selenium.remote.SessionId;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.crm.qa.base.TestBase;
//import com.crm.qa.pages.ContactsPage;

import com.crm.qa.pagesTestApp.HomePageTApp;
//import com.crm.qa.util.TestUtil;
import com.testingbot.testingbotrest.TestingbotREST;

import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;

public class HomePageTestApp extends TestBase {
	@SuppressWarnings("rawtypes")

	static HomePageTApp homePage;
	public static String build="1";

	@Test(priority = 1) public void VerifyHomePage_Test() throws
	  MalformedURLException, InterruptedException {
	  
	   iostestapp(); 
	  homePage = new
	  HomePageTApp(); // Thread.sleep(10000);
	  
	  System.out.println("Im in VerifyHomePage_Test"); Boolean homePageTitle =
	  homePage.verifyHomePage(); Assert.assertTrue(homePageTitle);
	  System.out.println("VerifyHomePage_Test Passed"); }

	@Test(priority = 2)
	public void VerifyChat_Test() throws MalformedURLException, InterruptedException {

		
		iostestapp();
		homePage = new HomePageTApp();
		System.out.println("Im in VerifyChat_Test");
		boolean tt = homePage.verifyChat();
		Assert.assertTrue(tt);
		System.out.println("VerifyChat_Test Passed");
	}

	/*@Test(priority = 3)
	public void VerifyEmoji_Test() throws InterruptedException, MalformedURLException {

		initialization();
		iostestapp();
		homePage = new HomePageTApp();

		System.out.println("Im in VerifyEmoji_Test");
		boolean tt = homePage.verifyEmoji();

		Assert.assertTrue(tt);
		System.out.println("VerifyEmoji_Test Passed");
	}

	@Test(priority = 4)
	public void VerifySticker_Test() throws InterruptedException, MalformedURLException {
		initialization();
		iostestapp();
		homePage = new HomePageTApp();

		System.out.println("Im in VerifySticker_Test");
		boolean tt = homePage.verifySticker();
		Assert.assertTrue(tt);
		System.out.println("VerifySticker_Test Passed");
	}

	@Test(priority = 5)
	public void VerifyMix_Emoji_Sticker_Text() throws InterruptedException, MalformedURLException {

		initialization();
		iostestapp();
		homePage = new HomePageTApp();
		System.out.println("Im in Verify_Mix_Emoji_Sticker_Text");
		boolean tt = homePage.verify_Mix_Emoji_Sticker_Text();
		Assert.assertTrue(tt);
		System.out.println("VerifyMix_Emoji_Sticker_Text Passed");
	}

	@Test(priority = 6)
	public void VerifyScrollingChat_Test() throws InterruptedException, MalformedURLException {

		initialization();
		iostestapp();
		homePage = new HomePageTApp();
		System.out.println("Im in VerifyScrollingChat_Test");
		boolean tt = homePage.verifyScrolling();
		Assert.assertTrue(tt);
		System.out.println("VerifyMix_Emoji_Sticker_Text Passed");
	}

	@Test(priority = 7)
	public void VerifyDuplicate_ChatLoad_Test() throws MalformedURLException, InterruptedException {
		initialization();
		iostestapp();
		homePage = new HomePageTApp();
		System.out.println("Im in VerifyDuplicate_ChatLoad_Test");
		boolean tt = homePage.Duplicate_Chat_Load();
		Assert.assertTrue(tt);
		System.out.println("VerifyDuplicate_ChatLoad_Test Passed");

	}

	@Test(priority = 8)
	public void VerifySnaptoLive_Test() throws InterruptedException, MalformedURLException {

		initialization();
		iostestapp();
		homePage = new HomePageTApp();
		System.out.println("Im in VerifySnaptoLive_Test");
		boolean tt = homePage.verifySnaptoLive();
		Assert.assertTrue(tt);
		System.out.println("VerifyDuplicate_ChatLoad_Test Passed");
	}

	@Test(priority = 9)
	public void VerifyBackground_ChatLoad_Test() throws MalformedURLException, InterruptedException {

		initialization();
		iostestapp();
		homePage = new HomePageTApp();
		System.out.println("Im in VerifyBackground_ChatLoad_Test");
		boolean tt = homePage.Background_Chat_Load();
		Assert.assertTrue(tt);
		System.out.println("VerifyBackground_ChatLoad_Test Passed");
	}

	@Test(priority = 10)
	public void VerifyVideo_Buffering_Test() throws InterruptedException, IOException {

		initialization();
		iostestapp();
		homePage = new HomePageTApp();

		System.out.println("Im in VerifyVideo_Buffering_Test");
		boolean tt = homePage.Video_Buffering();
		Assert.assertTrue(tt);
		System.out.println("VerifyVideo_Buffering_Test Passed");

	}

	
	/*
	 * @Test(priority = 12) public void VerifyVideo_Resume_Test() throws
	 * InterruptedException, IOException { testInfo =
	 * reports.createTest("VerifyVideo_Resume_Test"); initialization(); (new
	 * TouchAction(driver)).tap(new PointOption().withCoordinates(58,
	 * 179)).perform(); homePage = new HomePageTApp(); testInfo.log(Status.INFO,
	 * "Description: This test case checks if video is resumed when user comes from background"
	 * ); System.out.println("Im in VerifyVideo_Resume_Test"); boolean tt =
	 * homePage.Video_Resume(); Assert.assertTrue(tt); if (tt)
	 * testInfo.log(Status.INFO,
	 * "Expected Video Buffering GET METHOD Api Status after Relaunch: 200 || Actual Video Buffering GET METHOD Api Status after Relaunch : "
	 * + "200");
	 * 
	 * }
	 */
	  
	  
	  /*@Test(priority = 13) public void VerifyVideo_BufferingLandscape_Test() throws
	  InterruptedException, IOException {
	  
	  initialization(); driver.rotate(ScreenOrientation.LANDSCAPE); iostestapp();
	  homePage = new HomePageTApp();
	  System.out.println("Im in VerifyVideo_BufferingLandscape_Test"); boolean tt =
	  homePage.Video_Buffering_Landscape();
	  driver.rotate(ScreenOrientation.PORTRAIT); Assert.assertTrue(tt);
	  System.out.println("VerifyVideo_BufferingLandscape_Test Passed");
	  
	  }
	  
	  @Test(priority = 14) public void VerifyVideoLandscape_HideChatTest() throws
	  InterruptedException, IOException { initialization();
	  driver.rotate(ScreenOrientation.LANDSCAPE); iostestapp(); homePage = new
	  HomePageTApp();
	  System.out.println("Im in VerifyVideoLandscape_HideChatTest"); boolean tt =
	  homePage.VerifyVideoLandscape_HideChatTest();
	  driver.rotate(ScreenOrientation.PORTRAIT); Assert.assertTrue(tt);
	  System.out.println("VerifyVideoLandscape_HideChatTest Passed");
	  
	  }
	  
	  /*
	  
	  @Test(priority = 10) public void VerifyChat_CMS_Sync() throws
	  InterruptedException, IOException { testInfo =
	  reports.createTest("VerifyChat_CMS_Sync"); initialization(); (new
	  TouchAction(driver)).tap(new PointOption().withCoordinates(58,
	  179)).perform(); homePage = new HomePageTApp(); testInfo.log(Status.INFO,
	  "Description: This test case checks time sync between CMS and mobile app.");
	  System.out.println("Im in VerifyChat_CMS_Sync");
	  
	  // Thread.sleep(10000); System.out.println("Im in VerifyChat_Moderation");
	  boolean tt = homePage.VerifyChat_CMS_Sync();
	  
	  Assert.assertTrue(tt); if (tt)
	  
	  testInfo.log(Status.INFO,
	  "Expected Chat Sync Status : True || Actual Chat Sync Status : " + tt);
	  
	  }
	  
	  
	  /*
	  
	  @Test(priority = 16) public void VerifyChat_QueueingBypass_Test() throws
	  MalformedURLException, InterruptedException, ParseException { testInfo =
	  reports.createTest("VerifyChat_QueueingBypass_Test"); initialization(); (new
	  TouchAction(driver)).tap(new PointOption().withCoordinates(58,
	  179)).perform(); homePage = new HomePageTApp(); testInfo.log(Status.INFO,
	  "Description: This test case checks if chats are received on other device");
	  System.out.println("Im in VerifyChat_QueueingBypass_Test");
	  
	  boolean tt = homePage.Chat_Queueing_Bypass(); testInfo.log(Status.INFO,
	  "Expected Last Chat visibility on other mobile : True || Actual Last Chat visibility on other mobile : "
	  + tt); Assert.assertTrue(tt); }
	 

	/*
	 * @Test(priority = 17) public void VerifyAdsPlay_Test() throws
	 * MalformedURLException, InterruptedException {
	 * 
	 * initialization(); iostestapp(); homePage = new HomePageTApp();
	 * System.out.println("Im in VerifyAdsPlay_Test");
	 * 
	 * boolean tt = homePage.Ads(); Assert.assertTrue(tt);
	 * System.out.println("VerifyAdsPlay_Test Passed"); }
	 * 
	 * /*
	 * 
	 * @Test(priority = 18) public void SpoilerfreeSync() throws
	 * MalformedURLException, InterruptedException { testInfo =
	 * reports.createTest("SpoilerfreeSync"); initialization(); homePage = new
	 * HomePageTApp(); testInfo.log(Status.INFO,
	 * "Description: This test case checks sync between private chat rooms");
	 * System.out.println("Im in SpoilerfreeSync"); // Thread.sleep(10000); boolean
	 * tt = homePage.SpoilerfreeSync(); testInfo.log(Status.INFO,
	 * "Expected Spoiler free Sync : True || Actual Spoiler free Sync  : " + tt);
	 * Assert.assertTrue(tt); }
	 */

	/*
	 * @Test(priority = 20) public void KeyboardDismissOverride() throws
	 * MalformedURLException, InterruptedException {
	 * 
	 * initialization(); iostestapp(); homePage = new HomePageTApp();
	 * 
	 * System.out.println("Im in KeyboardDismissOverride");
	 * 
	 * boolean tt = homePage.KeyboardDismissOverride(); Assert.assertTrue(tt);
	 * System.out.println("KeyboardDismissOverride Passed"); }
	 * 
	 * @Test(priority = 21) public void RemoveMefromChatDisplayName() throws
	 * MalformedURLException, InterruptedException { initialization(); iostestapp();
	 * homePage = new HomePageTApp();
	 * System.out.println("Im in RemoveMefromChatDisplayName");
	 * 
	 * boolean tt = homePage.RemoveMefromChatDisplayName(); Assert.assertTrue(tt);
	 * System.out.println("KeyboardDismissOverride Passed"); }
	 * 
	 * /*
	 * 
	 * @Test(priority = 22) public void Profile_image() throws Exception { testInfo
	 * = reports.createTest("Profile_image"); initialization(); iostestapp(); (new
	 * TouchAction(driver)).tap(new PointOption().withCoordinates(58,
	 * 179)).perform(); homePage = new HomePageTApp(); testInfo.log(Status.INFO,
	 * "Description: This test case checks checks profile image of user.");
	 * System.out.println("Im in Profile_image");
	 * 
	 * boolean tt = homePage.Profile_image(); testInfo.log(Status.INFO,
	 * "Expected Profile Image status : True || Actual Profile Image status  : " +
	 * tt); Assert.assertTrue(tt); }
	 */

	/*
	 * @Test(priority = 23) public void WidgetOveride() throws InterruptedException,
	 * IOException, JSONException { testInfo = reports.createTest("WidgetOveride");
	 * initialization(); iostestapp(); //
	 * 
	 * homePage = new HomePageTApp(); testInfo.log(Status.INFO,
	 * "Description: This test case checks that different animation is coming for widgets"
	 * ); System.out.println("Im in WidgetOveride");
	 * 
	 * boolean tt = homePage.WidgetOveridee(); testInfo.log(Status.INFO,
	 * "Expected WidgetOveride status : True || Actual WidgetOveride status  : " +
	 * tt); Assert.assertTrue(tt); }
	 */

	/*
	 * @Test(priority = 24) public void UnreadMessageCount() throws
	 * MalformedURLException, InterruptedException { testInfo =
	 * reports.createTest("UnreadMessageCount"); initialization(); homePage = new
	 * HomePageTApp(); testInfo.log(Status.INFO,
	 * "Description: This test case checks that correct unread message count is shown"
	 * ); System.out.println("Im in UnreadMessageCount");
	 * 
	 * boolean tt = homePage.UnreadMessageCount(); testInfo.log(Status.INFO,
	 * "Expected UnreadMessageCount status : True || Actual UnreadMessageCount status  : "
	 * + tt); Assert.assertTrue(tt); }
	 */

	/*
	 * @Test(priority = 25) public void Turner_ExternalGIFS_Bitmoji() throws
	 * MalformedURLException, InterruptedException { initialization(); iostestapp();
	 * homePage = new HomePageTApp();
	 * 
	 * System.out.println("Im in Turner_ExternalGIFS_Bitmoji");
	 * 
	 * boolean tt = homePage.External_GIfs();
	 * 
	 * Assert.assertTrue(tt); }
	 * 
	 * @Test(priority = 26) public void Turner_ExternalGIFS_Gipphy() throws
	 * MalformedURLException, InterruptedException {
	 * 
	 * initialization(); iostestapp(); homePage = new HomePageTApp();
	 * 
	 * System.out.println("Im in Turner_ExternalGIFS_Bitmoji");
	 * 
	 * boolean tt = homePage.External_GIfs_Giffy();
	 * 
	 * Assert.assertTrue(tt); }
	 * 
	 * @Test(priority = 27) public void Turner_Chat_Reaction() throws
	 * MalformedURLException, InterruptedException {
	 * 
	 * initialization(); // iostestapp(); homePage = new HomePageTApp();
	 * 
	 * System.out.println("Im in Turner_Chat_Reaction");
	 * 
	 * boolean tt = homePage.Turner_Chat_Reaction(); Assert.assertTrue(tt); }
	 * 
	 * @Test(priority = 28) public void Turner_SetUsername_Reaction() throws
	 * MalformedURLException, InterruptedException { initialization(); iostestapp();
	 * homePage = new HomePageTApp();
	 * System.out.println("Im in Turner_SetUsername_Reaction");
	 * 
	 * boolean tt = homePage.Turner_SetUsername_Reaction(); Assert.assertTrue(tt); }
	 * 
	 * @Test(priority = 29) public void EmptyChatDefaultView() throws
	 * MalformedURLException, InterruptedException {
	 * 
	 * initialization(); homePage = new HomePageTApp();
	 * System.out.println("Im in EmptyChatDefaultView"); boolean tt =
	 * homePage.EmptyChatDefaultView(); Assert.assertTrue(tt); }
	 */

}

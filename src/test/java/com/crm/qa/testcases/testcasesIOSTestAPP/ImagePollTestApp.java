package com.crm.qa.testcases.testcasesIOSTestAPP;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.json.JSONException;
import org.openqa.selenium.ScreenOrientation;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.crm.qa.base.TestBase;
import com.crm.qa.widgets.ImagePollApi;
import com.crm.qa.widgets.TextPollApi;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.touch.offset.PointOption;


public class ImagePollTestApp extends TestBase{
	
	@SuppressWarnings("rawtypes")
//	public static AppiumDriver driver;
//	static int x;
//	static int y;
	
	
	
	
	
	
	@Test
	public static void Imagepoll_Widget_Test_Potrait() throws InterruptedException, IOException, JSONException{


		
		
		
		
		//initialization();
		
		System.out.println("Im in Imagepoll_Widget_Test_Potrait");
		driver.rotate(ScreenOrientation.PORTRAIT);
		iostestapp();
		
		
		if (driver.findElementsByXPath(
				"//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[5]/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell")
				.size() > 0) {
		ImagePollApi.authcheck2();
		
		driver.findElementByXPath("//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[4]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeButton[2]").click();
		ImagePollApi.authcheck3();
		
		}
		else {
			System.out.println("Chats not loaded");
			Assert.assertTrue(false);
		}
		
	}
	
	
	
	@Test
	public static void Imagepoll_Widget_Test_Landscape() throws InterruptedException, IOException, JSONException{


		
		
		
		
		
		//initialization();
		iostestapp();
		Thread.sleep(1000);
		driver.rotate(ScreenOrientation.LANDSCAPE);
		System.out.println("Im in Imagepoll_Widget_Test_Landscape");
		
		
		if (driver.findElementsByXPath(
				  "//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[4]/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell")
					.size() > 0) {
		
		
		ImagePollApi.authcheck2();
		
		driver.findElementByXPath("//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeButton[2]").click();
		ImagePollApi.authcheck3();
		
		
		driver.rotate(ScreenOrientation.PORTRAIT);
		}
		else {
			System.out.println("Chats not loaded");
			Assert.assertTrue(false);
		}
	}
	
	

}

package com.crm.qa.testcases.testcasesIOSTestAPP;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
//import java.time.Duration;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.json.JSONException;
import org.openqa.selenium.Point;
import org.openqa.selenium.ScreenOrientation;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.crm.qa.base.TestBase;
import com.crm.qa.widgets.ImageQuizApi;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.ShakesDevice;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;

public class ImageQuizTestApp extends TestBase {
	@SuppressWarnings("rawtypes")

	@Test
	public static void ImageQuiz_Widget_Test_Potrait() throws InterruptedException, IOException, JSONException {

		//initialization();
		driver.rotate(ScreenOrientation.PORTRAIT);
		iostestapp();
		System.out.println("Im in ImageQuiz_Widget_Test");

		if (driver.findElementsByXPath(
				"//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[5]/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell")
				.size() > 0) {

			ImageQuizApi.authcheck();

			driver.findElementByXPath(
					"//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[4]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeButton[2]")
					.click();

			ImageQuizApi.authcheck2();
		} else {
			System.out.println("Chats not loaded");
			Assert.assertTrue(false);
		}

	}

	@Test
	public static void ImageQuiz_Widget_Test_Landscape() throws InterruptedException, IOException, JSONException {

		
		//initialization();
		iostestapp();
		  Thread.sleep(1000);
		  driver.rotate(ScreenOrientation.LANDSCAPE);
		System.out.println("Im in ImageQuiz_Widget_Test_Landscape");
		if (driver.findElementsByXPath(
				  "//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[4]/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell")
					.size() > 0) {
		
		ImageQuizApi.authcheck();
		

		driver.findElementByXPath(
				"//XCUIElementTypeApplication[@name=\"LiveLikeTestApp\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeButton[2]")
				.click();

		ImageQuizApi.authcheck2();

		
		driver.rotate(ScreenOrientation.PORTRAIT);
		}
		else {
			System.out.println("Chats not loaded");
			Assert.assertTrue(false);
		}

	}

}
